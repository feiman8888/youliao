package com.seahorse.youliao.vo.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;

/**
 * @ProjectName: youliao
 * @Package: com.seahorse.youliao.vo.request
 * @ClassName: UserRegisterVO
 * @Description: 用户注册
 * @author:songqiang
 * @Date:2020-11-27 10:55
 **/
@ApiModel()
@Getter
@Setter
@ToString
public class UserRegisterVO {

    /**
     * 邮箱
     */
    @ApiModelProperty("邮箱")
    @NotBlank(message = "邮箱不能为空")
    private String email;

    /**
     * 密码
     */
    @ApiModelProperty("密码")
    @NotBlank(message = "密码不能为空")
    private String password;

    /**
     * 手机号
     */
    @ApiModelProperty("手机号")
    @NotBlank(message = "手机号不能为空")
    private String mobile;

    /**
     * 验证码
     */
    @ApiModelProperty("验证码")
    @NotBlank(message = "验证码不能为空")
    private String code;


}
