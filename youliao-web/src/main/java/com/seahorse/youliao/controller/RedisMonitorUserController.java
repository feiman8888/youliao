package com.seahorse.youliao.controller;

import com.google.common.collect.Lists;
import com.seahorse.youliao.security.JwtUserRedis;
import com.seahorse.youliao.utils.PageListUtil;
import com.seahorse.youliao.vo.request.SysUserQueryVO;
import com.seahorse.youliao.vo.response.RedisMonitorUserResponseVO;
import com.zengtengpeng.operation.RedissonObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.redisson.api.RKeys;
import org.redisson.api.RedissonClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @ProjectName: youliao
 * @Package: com.seahorse.youliao.controller
 * @ClassName: RedissonClientController
 * @Description: redisson 客户端操作缓存数据
 * @author:songqiang
 * @Date:2020-11-08 17:25
 **/
@Api(value = "RedisMonitorUserController", tags = "redis 监控用户登录信息")
@Controller
@RequestMapping(value = "/redis/user")
public class RedisMonitorUserController {


    private static Logger logger = LoggerFactory.getLogger(RedisMonitorUserController.class);

    /**
     * 原始redisClient
     */
    @Resource
    private RedissonClient redissonClient;

    /**
     * 操作对象
     */
    @Resource
    private RedissonObject redissonObject;

    /**
     * 查询所有登录用户缓存token数据分页展示
     * @return
     */
    @ApiOperation(value = "查询所有登录用户缓存token数据分页展示")
    @PostMapping("/getList")
    @ResponseBody
    public RedisMonitorUserResponseVO getObjectList(@RequestBody @Valid SysUserQueryVO queryVO)  {


        logger.info("查询所有登录用户缓存token数据分页展示");
        RKeys keys = redissonClient.getKeys();
        Iterable<String> iterable = keys.getKeysByPattern("user:*");
        List<String> list = Lists.newArrayList(iterable);

        List<JwtUserRedis> userList = new ArrayList<>();
        for (String s : list) {

            JwtUserRedis vo = redissonObject.getValue(s);
            userList.add(vo);
        }

        Collections.sort(userList, new Comparator<JwtUserRedis>() {
            @Override
            public int compare(JwtUserRedis o1, JwtUserRedis o2) {
                //倒序
                return o2.getLoginTime().compareTo(o1.getLoginTime());
            }
        });

         List<JwtUserRedis> userPageList = PageListUtil.startPage(userList, queryVO.getPageNum(), queryVO.getPageSize());
        RedisMonitorUserResponseVO responseVO = new RedisMonitorUserResponseVO();
        responseVO.setList(userPageList);
        responseVO.setTotal(Long.valueOf(list.size()));
        responseVO.setPageNum(queryVO.getPageNum());
        responseVO.setPageSize(queryVO.getPageSize());
        return responseVO;
    }


}
