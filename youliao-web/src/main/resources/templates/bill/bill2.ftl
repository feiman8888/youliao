﻿﻿<html xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8">
    <title>bill</title>
    <script type="text/javascript" src="/api/js/X-doc.js"></script>
</head>
<body style="height:100%; margin:0; overflow:hidden;">
<script id="myxdoc" type="text/xdoc" _format="pdf" style="width:100%;height:100%;">
    <xdoc version="A.3.0">
    <paper margin="0" width="300" height="500" />
    <body padding="10" fillImg="#@f40">
        <para align="left" lineSpacing="15">
            <text fontName="行楷" fontSize="10" fontStyle="shadow">流水号:12321312</text>
        </para>
        <para align="center" lineSpacing="0" fillColor="#ff99ff" name="x6870" width="300">
            <text  fontSize="8">---------------------------------------------------------------------</text>
        </para>

        <para align="center" lineSpacing="5">
            <text  fontName="粗黑" fontSize="18">有料集团销售凭证</text>
        </para>

        <para>
            <rect color="" name="x6870" width="100">
                <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6870" width="100">
                    <text fontName="黑体" fontSize="13">计划编号</text>
                </para>
            </rect>
            <rect color="" name="x6871" width="150">
                <para align="center" lineSpacing="3" fillColor="#ff99ff" name="x6871" width="150">
                    <text fontName="仿宋" fontSize="13">NO1123</text>
                </para>
            </rect>
        </para>
        <para>
            <rect color="" name="x6870" width="100">
                <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6870" width="100">
                    <text fontName="黑体" fontSize="13">提货产品</text>
                </para>
            </rect>
            <rect color="" name="x6871" width="150">
                <para align="center" lineSpacing="3" fillColor="#ff99ff" name="x6871" width="150">
                    <text  fontName="仿宋" fontSize="13"> 松茸 </text>
                </para>
            </rect>
        </para>
        <para>
            <rect color="" name="x6870" width="100">
                <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6870" width="100">
                    <text fontName="黑体" fontSize="13">车牌号</text>
                </para>
            </rect>
            <rect  color="" name="x6871" width="150">
                <para align="center" lineSpacing="3" fillColor="#ff99ff" name="x6871" width="150">
                    <text  fontName="仿宋" fontSize="13">川A09C6K</text>
                </para>
            </rect>
        </para>
        <para>
            <rect color="" name="x6870" width="100">
                <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6870" width="100">
                    <text fontName="黑体" fontSize="13">办理人</text>
                </para>
            </rect>
            <rect  color="" name="x6871" width="150">
                <para align="center" lineSpacing="3" fillColor="#ff99ff" name="x6871" width="150">
                    <text  fontName="仿宋" fontSize="13" >张三三</text>
                </para>
            </rect>
        </para>
        <para>
            <rect color="" name="x6870" width="100">
                <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6870" width="100">
                    <text fontName="黑体" fontSize="13">交付金额</text>
                </para>
            </rect>
            <rect  color="" name="x6871" width="150">
                <para align="center" lineSpacing="3" fillColor="#ff99ff" name="x6871" width="150">
                    <text  fontName="仿宋" fontSize="13">$9000.00</text>
                </para>
            </rect>
        </para>
        <para>
            <rect color="" name="x6870" width="100">
                <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6870" width="100">
                    <text fontName="黑体" fontSize="13">手机号</text>
                </para>
            </rect>
            <rect  color="" name="x6871" width="150">
                <para align="center" lineSpacing="3" fillColor="#ff99ff" name="x6871" width="150">
                    <text  fontName="仿宋" fontSize="13">18888888888</text>
                </para>
            </rect>
        </para>
        <para>
            <rect color="" name="x6870" width="100">
                <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6870" width="100">
                    <text fontName="黑体" fontSize="13">到达时间</text>
                </para>
            </rect>
            <rect  color="" name="x6871" width="150">
                <para align="center" lineSpacing="3" fillColor="#ff99ff" name="x6871" width="150">
                    <text  fontName="仿宋" fontSize="13">2020-01-01</text>
                </para>
            </rect>
        </para>


        <para  align="center"  >
            <#--二维码-->
            <img height="150" width="150" drawType="adjust"
                     src="http://localhost:8080/qrCode.png"/>
        </para>
        <para align="center" lineSpacing="15">
            <text fontName="黑体" fontSize="12" >二维码编号</text>
        </para>
        <para align="center" lineSpacing="15">
            <text fontName="仿宋" fontSize="12" >151543265451-1</text>
        </para>
        <para align="center" lineSpacing="0" fillColor="#ff99ff" name="x6870" width="300">
            <text  fontSize="8">---------------------------------------------------------------------</text>
        </para>
        <para align="left" lineSpacing="15">
            <text fontName="楷体" fontSize="10" fontStyle="shadow">打印时间:2019-12-30 19:41:00</text>
        </para>

    </body>
    </xdoc>
</script>
</body>
</html>
