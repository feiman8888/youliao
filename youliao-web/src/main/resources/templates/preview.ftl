﻿﻿<html xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8"/>
    <script type="text/javascript" src="/api/js/xdoc.js"></script>
</head>
<body style="height:100%; margin:0; overflow:hidden;">
<input type="hidden" id="xdocType" value="${type!}" />
<input type="hidden" id="loadId" value="${loadId!}" />
<script id="myxdoc" type="text/xdoc" _format="pdf" style="width:100%;height:100%;">
    <xdoc version="11.1.5">
        <meta modifyDate="2017-08-15 14:45:10" author="Administrator" id="3ulahulfbrdwnfujnnjulqjcpm" title="合同预览" createDate="2017-08-15 12:37:46"/>
        <paper width="812" height="1124" topMargin="15" bottomMargin="15" leftMargin="61" rightMargin="71"/>
        <body>
            ${context}
        </body>
    </xdoc>
</script>
<script>

    (function disableRightClick() {
        if (document.layers) {
            document.captureEvents(Event.MOUSEDOWN);
        }
        document.onmousedown = click;
        document.oncontextmenu = new Function("return false;")
        function click(e) {
            e = e || event;
            if (e.button == 2) {
                var tag = e.srcElement || e.target;
                if (tag.type == "text" || tag.type == "textarea") {
                    document.oncontextmenu = new Function("return true;")
                } else {
                    document.oncontextmenu = new Function("return false;")
                }
            }
        }
    })();

</script>
</body>
</html>